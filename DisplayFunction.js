const apiKey = "7e4fbd45c17544cce5fbbd89902f5b8d";
const baseUrlImage = "https://image.tmdb.org/t/p/";
const defaultSize = "w500";
//add spinner
// get top rated movies from API of themoviedb
async function GetTopRated(page)
{
    $("#spinner").show();
    $("#movieDetails").empty();
    $("#personDetails").empty();
    $("#cardHeader").empty();
    $("#movies").empty();
    $("#pagination").empty();
    $("#review").empty();
    const reponse = await fetch(`https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=${page}`); 
    const topMovies = await reponse.json();
    

    $("#cardHeader").append
    (`<h5>Top Rated</h5>`);
    $("#spinner").hide();
    for (movie of topMovies.results)
    {
        $("#movies").append
        (`
        <div class="cardMovies rounded-0 bg-dark" onclick="MovieDetails(${movie.id})">
            <img class="card-img-top" src="${baseUrlImage}${defaultSize}${movie.poster_path}"
                style="width:100%"  alt="${movie.title}">
            <div class="card-body">
                <h5 class="card-title">${movie.title}</h5>
                <p class="card-text">Total vote: ${movie.vote_count}</p>
                <p class="card-text">Rate: ${movie.vote_average}</p>
            </div>
        </div>
        `);
    }
    AddPagination("GetTopRated",page);
}

async function GetNowPlaying(page)
{
    $("#spinner").show();
    $("#movieDetails").empty();
    $("#personDetails").empty();
    $("#cardHeader").empty();
    $("#movies").empty();
    $("#pagination").empty();
    $("#review").empty();
    const reponse = await fetch(`https://api.themoviedb.org/3/movie/now_playing?api_key=${apiKey}&language=en-US&page=${page}`); 
    const nowMovies = await reponse.json();
    console.log(nowMovies); 
    
    
    $("#cardHeader").append
    (`<h5>Now playing</h5>`);
    $("#spinner").hide();
    for (movie of nowMovies.results)
    {
        $("#movies").append
        (`
        <div class="cardMovies rounded-0 bg-dark"onclick="MovieDetails(${movie.id})">
            <img class="card-img-top" src="${baseUrlImage}${defaultSize}${movie.poster_path}"
                style="width:100%"  alt="${movie.title}">
            <div class="card-body">
                <h5 class="card-title">${movie.title}</h5>
                <p class="card-text">Release: ${movie.release_date}</p>
                <p class="card-text">Rate: ${movie.vote_average}</p>
            </div>
        </div>
        `);
    }
    AddPagination("GetNowPlaying", page);
}   

async function GetPopular(page)
{
    $("#spinner").show();
    $("#movieDetails").empty();
    $("#personDetails").empty();
    $("#cardHeader").empty();
    $("#movies").empty();
    $("#pagination").empty();
    $("#review").empty();
    const reponse = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=${page}`); 
    const popularMovies = await reponse.json();
    
    $("#cardHeader").append
    (`<h5>Popular</h5>`);
    $("#spinner").hide();
    for (movie of popularMovies.results)
    {
        $("#movies").append
        (`
        <div class="cardMovies rounded-0 bg-dark"onclick="MovieDetails(${movie.id})">
            <img class="card-img-top" src="${baseUrlImage}${defaultSize}${movie.poster_path}"
                style="width:100%"  alt="${movie.title}">
            <div class="card-body">
                <h5 class="card-title">${movie.title}</h5>
                <p class="card-text">Popularity: ${movie.popularity}</p>
                <p class="card-text">Rate: ${movie.vote_average}</p>
            </div>
        </div>
        `);
    }
    AddPagination("GetPopular",page);
}

async function GetUpcoming(page)
{
    $("#spinner").show();
    $("#movieDetails").empty();
    $("#personDetails").empty();
    $("#cardHeader").empty();
    $("#movies").empty();
    $("#pagination").empty();
    $("#review").empty();
    const reponse = await fetch(`https://api.themoviedb.org/3/movie/upcoming?api_key=${apiKey}&language=en-US&page=${page}`); 
    const upcomingMovies = await reponse.json();
    
    
    $("#cardHeader").append
    (`<h5>UpComing</h5>`);
    $("#spinner").hide();
    for (movie of upcomingMovies.results)
    {
        $("#movies").append
        (`
        <div class="cardMovies rounded-0 bg-dark"onclick="MovieDetails(${movie.id})">
            <img class="card-img-top" src="${baseUrlImage}${defaultSize}${movie.poster_path}"
                style="width:100%"  alt="${movie.title}">
            <div class="card-body">
                <h5 class="card-title">${movie.title}</h5>
                <p class="card-text">Release: ${movie.release_date}</p>
                <p class="card-text">Rate: ${movie.vote_average}</p>
            </div>
        </div>
        `);
    }
    AddPagination("GetUpcoming",page);
}

async function SearchMovies(page)
{
    $("#movieDetails").empty();
    $("#personDetails").empty();
    $("#bodySite").empty();
    $("#pagination").empty();
    $("#review").empty();
    $("#spinner").show();
    let searchString = document.getElementById('searchData').value;
    const reponse = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&language=en-US&query=${searchString}&page=${page}`);
    const result = await reponse.json();
    
    $("#bodySite").append
    (`
    <div class="card text-white bg-dark rounded-0 mb-3" style="max-width: 100%;">
        <div class="card-header" id="cardHeader">Result</div>
        <div class="card-body" id="movies" style="display:inline-flex; flex-wrap: wrap;">
        </div>
    </div>
    `);
    $("#spinner").hide();
    for (movie of result.results)
    {
        $("#movies").append
        (`
        <div class="cardMovies rounded-0 bg-dark" onclick="MovieDetails(${movie.id})">
            <img class="card-img-top" src="${baseUrlImage}${defaultSize}${movie.poster_path}"
                style="width:100%; height:500px"  alt="${movie.title}">
            <div class="card-body">
                <h5 class="card-title">${movie.title}</h5>
                <p class="card-text">Release: ${movie.release_date}</p>
                <p class="card-text">Rate: ${movie.vote_average}</p>
            </div>
        </div>
        `);
    }
    AddPagination("SearchMovies",page);
}

async function SearchActor(page)
{
    $("#movieDetails").empty();
    $("#personDetails").empty();
    $("#bodySite").empty();
    $("#pagination").empty();
    $("#review").empty();
    $("#spinner").show();
    let searchString = document.getElementById('searchData').value;
    const reponse = await fetch(`http://api.themoviedb.org/3/search/person?api_key=${apiKey}&language=en-US&query=${searchString}&page=${page}`);
    const result = await reponse.json();
    
    $("#bodySite").append
    (`
    <div class="card text-white bg-dark rounded-0 mb-3" style="max-width: 100%;">
        <div class="card-header" id="cardHeader"><h5>Result<h5></div>
        <div class="card-body" id="movies" style="display:inline-flex; flex-wrap: wrap;">
        </div>
    </div>
    `);
    $("#spinner").hide();
    console.log(result);
    for (const career of result.results)
    {

        if (career.known_for_department === "Acting")
        {
            console.log(career);
            for (movie of career.known_for)
            {
                if (movie.media_type === "movie")
                {
                    $("#movies").append
                    (`
                    <div class="cardMovies rounded-0 bg-dark" onclick="MovieDetails(${movie.id})">
                        <img class="card-img-top" src="${baseUrlImage}${defaultSize}${movie.poster_path}"
                            style="width:100%; height:500px"  alt="${movie.title}">
                        <div class="card-body">
                            <h5 class="card-title">${movie.title}</h5>
                            <p class="card-text">Release: ${movie.release_date}</p>
                            <p class="card-text">Rate: ${movie.vote_average}</p>
                        </div>
                    </div>
                    `);
                }
            }
        }
    }

    AddPagination("SearchActor",page);
}
let arrayCredit = [];
async function MovieDetails(movie_id)
{
    $("#spinner").show();
    $("#movieDetails").empty();
    $("#personDetails").empty();
    $("#cardHeader").empty();
    $("#movies").empty();
    $("#pagination").empty();
    $("#review").empty();
    const reponseMovie = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}?api_key=${apiKey}&language=en-US`);
    const reponseCast = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/credits?api_key=${apiKey}`);
    const reponseReview = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/reviews?api_key=${apiKey}&language=en-US`);
    const movieDetails = await reponseMovie.json();
    const creditsMovie = await reponseCast.json();
    const movieReview = await reponseReview.json();
    gener = "";
    $("#spinner").hide();
    for (const genres of movieDetails.genres) {
        gener = `${gener}<div style="display: inline-block;margin:5px">${genres.name}</div>`;
    }
    $("#movieDetails").append
    (`
    <div class="card bg-dark text-white" style="margin-bottom: 10px">
        <img class="card-img" src="${baseUrlImage}original${movieDetails.backdrop_path}" style="mix-blend-mode:darken"
        alt="${movieDetails.title}">
        <div class="card-img-overlay detail">
            <img class="detailImage col-md-5" src="${baseUrlImage}${defaultSize}${movieDetails.poster_path}"
            alt="${movieDetails.original_title}">
            <div class="detailText">
                <h3>${movieDetails.title}</h3>
                <h5>(${movieDetails.original_title})</h5>
                <p>Release Date: ${movieDetails.release_date}.</p>
                <p>Rate: ${movieDetails.vote_average}.</p>
                <p>Length: ${movieDetails.runtime} minutes.</p>
                <div>Genres: ${gener}</div>
                <p></p>
                <p id="director"></p>
                <p>Overview: ${movieDetails.overview}</p>
            </div>
        </div>
    </div>
    </div>
    `)
    for (crew of creditsMovie.crew)
    {
        if (crew.department == "Directing")
        {
            $("#director").append
            (`Directtor: ${crew.name}`);
        }
    }
    $("#cardHeader").append
    (`<h5>Cast</h5>`);
    $("#movies").append
    (`
    <div class="scrollActor" id="scrollActor">

    </div>
    `);
    for (let actor of creditsMovie.cast)
    {
        let castinfor = {id: actor.id, credit_id: actor.credit_id};
        arrayCredit.push(castinfor);
        $("#scrollActor").append
        (`
        <div class="card rounded-0 bg-dark col-md-2" onclick="PersonDetails(${actor.id})">
            <img class="card-img-top" src="${baseUrlImage}${defaultSize}${actor.profile_path}"
                alt="${actor.name}">
            <div class="card-body">
                <h5 class="card-title">${actor.name}</h5>
                <p class="card-text">Character: ${actor.character}</p>
            </div>
        </div>
        `);
    }
    $("#review").append
    (`
    <h3><b>Review</b></h3>
    <div class="scrollReview" id="scroolView">
    </div>
    `);
    for (review of movieReview.results)
    {
        $("#scroolView").append
        (`
        <div class="card" style="width: 100%;">
            <div class="card-body">
                <h5 class="card-title">${review.author}</h5>
                <p class="card-text">${review.content}</p>
            </div>
        </div>
        `);
    }

}

async function PersonDetails(person_id)
{
    $("#spinner").show();
    $("#movieDetails").empty();
    $("#personDetails").empty();
    $("#cardHeader").empty();
    $("#movies").empty();
    $("#pagination").empty();
    $("#review").empty();
    const reponseInfor = await fetch(`https://api.themoviedb.org/3/person/${person_id}?api_key=${apiKey}&language=en-US`);
    for (credit of arrayCredit)
    {
        if (credit.id == person_id)
        {
            creditID = credit.credit_id;
            break;
        }
    }
    const reponseRelativeMovies = await fetch(`https://api.themoviedb.org/3/credit/${creditID}?api_key=${apiKey}`);
    const personInfor = await reponseInfor.json();
    const relativeMovies = await reponseRelativeMovies.json();
    console.log(personInfor);
    console.log(relativeMovies);
    $("#spinner").hide();
    let nickName ="";
    for (name of personInfor.also_known_as)
    {
        nickName = nickName +", " + name;
    }
    $("#personDetails").append
    (`
    <div class="detail">
            <img class="detailImage col-md-5" src="${baseUrlImage}${defaultSize}${personInfor.profile_path}"
            alt="${personInfor.name}">
            <div class="detailText">
                <h3>${personInfor.name}</h3>
                <h5>Other name: ${nickName}</h5>
                <p>Birthday: ${personInfor.birthday}.</p>
                <p>Born: ${personInfor.place_of_birth}.</p>
                
                <p>Biography: ${personInfor.biography}</p>
            </div>
        </div>
    `);
    $("#cardHeader").append
    (`<h5>Movie participated</h5>`);
    for (relativemovie of relativeMovies.person.known_for)
    {
        console.log(relativemovie);
        if (relativemovie.media_type == "movie")
        {
            $("#movies").append
            (`
            <div class="card rounded-0 bg-dark col-md-2" onclick="MovieDetails(${relativemovie.id})">
                <img class="card-img-top" src="${baseUrlImage}${defaultSize}${relativemovie.poster_path}"
                    alt="${relativemovie.original_title}">
                <div class="card-body">
                    <h5 class="card-title">${relativemovie.original_title}</h5>
                    <p class="card-text">Release: ${relativemovie.release_date}</p>
                </div>
            </div>
            `);
        }

    }
}

function AddPagination(funcName,page)
{
    $("#pagination").empty(); 
    if (page == 1)
    {
        $("#pagination").append
        (`
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#" onclick="${funcName}(${page})">${page}</a></li>
                    <li class="page-item"><a class="page-link" href="#" onclick="${funcName}(${page+1})">${page+1}</a></li>
                    <li class="page-item"><a class="page-link" href="#" onclick="${funcName}(${page+2})">${page+2}</a></li>
                    <li class="page-item">
                    <a class="page-link" href="#" onclick="${funcName}(${page+1})">Next</a>
                    </li>
                </ul>
            </nav>
        `);
    }
    else if (page == movie.total_pages-2)
    {
        $("#pagination").append
        (`
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                    <a class="page-link" href="#" onclick="${funcName}(${page-1})">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#" onclick="${funcName}(${page})">${page}</a></li>
                    <li class="page-item"><a class="page-link" href="#" onclick="${funcName}(${page+1})">${page+1}</a></li>
                    <li class="page-item"><a class="page-link" href="#" onclick="${funcName}(${page+2})">${page+2}</a></li>
                    <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true")>Next</a>
                    </li>
                </ul>
            </nav>
        `);
    }
    else
    {
        $("#pagination").append
        (`
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                    <a class="page-link" href="#" onclick="${funcName}(${page-1})">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#" onclick="${funcName}(${page})">${page}</a></li>
                    <li class="page-item"><a class="page-link" href="#" onclick="${funcName}(${page+1})">${page+1}</a></li>
                    <li class="page-item"><a class="page-link" href="#" onclick="${funcName}(${page+2})">${page+2}</a></li>
                    <li class="page-item">
                    <a class="page-link" href="#" onclick="${funcName}(${page+1})">Next</a>
                    </li>
                </ul>
            </nav>
        `);
    }
}